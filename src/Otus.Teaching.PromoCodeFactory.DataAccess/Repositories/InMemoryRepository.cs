﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data) =>
            Data = data;

        public Task<ICollection<T>> GetAllAsync() =>
            Task.FromResult(Data);

        public Task<T> GetByIdAsync(Guid id) =>
            Task.FromResult(Data.FirstOrDefault(x => x.Id == id));

        public Task InsertAsync(T entity) =>
            Task.Run(() => Data.Add(entity));

        public Task<bool> UpdateAsync(T entity) =>
            Task.Run(() =>
            {
                var result = Data.SingleOrDefault(e => e.Id == entity.Id);
                if (result == null)
                    return false;

                Data.Remove(result);
                Data.Add(entity);   
                return true;
            });

        public Task<bool> DeleteAsync(Guid id) =>
            Task.Run(() =>
            {
                var result = Data.SingleOrDefault(e => e.Id == id);
                if (result == null)
                    return false;
                return Data.Remove(result);
            });
    }
}