﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>Сотрудники</summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository
        )
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
   
        /// <summary>Получить данные всех сотрудников</summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>Получить данные сотрудника по Id</summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync([FromBody] EmployeeShortRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest("Передан пустой объект");

            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                Roles = (await _roleRepository.GetAllAsync())
                    .Where( r => employeeRequest.Roles.Contains(r.Id))
                    .ToList()
            };

            await _employeeRepository.InsertAsync(employee);
            return Ok(employee.Id);
        }


        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync([FromBody] EmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest("Передан пустой объект");

            var employee = new Employee()
            {
                Id = employeeRequest.Id,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
                Roles = (await _roleRepository.GetAllAsync())
                    .Where(r => employeeRequest.Roles.Contains(r.Id))
                    .ToList()
            };

            var isUpdated = await _employeeRepository.UpdateAsync(employee);
            if(isUpdated)
                return Ok(employee.Id);
            return NotFound();
        }


        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> DeleteEmployeeAsync(Guid id)
        {
            var isDeleted = await _employeeRepository.DeleteAsync(id);

            if (isDeleted)
                return Ok(id);
            return NotFound();
        }
    }
}